//
//  IdiotViewController.m
//  Idiot
//
//  Created by preetham d'souza on 1/24/12.
//  Copyright 2012 techjini. All rights reserved.
//

#import "IdiotViewController.h"

@implementation IdiotViewController

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIView *homeView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.height, self.view.bounds.size.width)];
    
    UILabel *startLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 30, 270, 60)];
    startLabel.text = @"press '5' to start the game:";
    [homeView addSubview:startLabel];
    [startLabel release];
    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(190, 120, 100, 20) ;
    //btn1.backgroundColor = [UIColor redColor];
    [btn1 setBackgroundColor:[UIColor redColor]];
    [btn1 setTitle:@"START" forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(idiot) forControlEvents:UIControlEventTouchUpInside];
    [homeView addSubview:btn1];
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(320, 250, 100, 20) ;
    [btn2 setBackgroundColor:[UIColor blueColor]];
    [btn2 setTitle:@"Next" forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(idiot) forControlEvents:UIControlEventTouchUpInside];
    [homeView addSubview:btn2];
    
    UIButton *btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn3.frame = CGRectMake(20, 250, 100, 20) ;
    //[btn3 setBackgroundColor:[UIColor blueColor]];
    [btn3 setTitleColor:[UIColor brownColor] forState:UIControlStateNormal];
    [btn3 setTitle:@"5" forState:UIControlStateNormal];
    [btn3 addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
    [homeView addSubview:btn3];
    
    
    [self.view addSubview:homeView];
    [homeView release];
}

- (void)idiot {
    NSLog(@"idiot :))))))");
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"You are an Idiot !!!"  message:@"idiot You Lost" delegate:nil cancelButtonTitle:@"I Agree" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

- (void)next {
    NSLog(@"NEXT :{");
    NSArray *views =  [self.view subviews];
    UIView *view = nil;
    for (view in views) {
        [view removeFromSuperview];
    }
    
    UIView *homeView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    
    UILabel *startLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 10, 270, 60)];
    startLabel.text = @"press circle to start the game:";
    [homeView addSubview:startLabel];
    [startLabel release];
    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(230, 70, 200, 100) ;
    [btn1 setImage:[UIImage imageNamed:@"Rectangle.png"] forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(idiot) forControlEvents:UIControlEventTouchUpInside];
    [homeView addSubview:btn1];
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(10, 70, 200, 100) ;
    [btn2 setImage:[UIImage imageNamed:@"gradient-oval.png"] forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(idiot) forControlEvents:UIControlEventTouchUpInside];
    [homeView addSubview:btn2];
    
    UIButton *btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn3.frame = CGRectMake(30, 175, 176, 144) ;
    [btn3 setImage:[UIImage imageNamed:@"circle1.png"] forState:UIControlStateNormal];
    [btn3 addTarget:self action:@selector(next1) forControlEvents:UIControlEventTouchUpInside];
    [homeView addSubview:btn3];
    
    
    
    [self.view addSubview:homeView];
    [homeView release];
    
}

-(void)next1 {
    
    NSArray *views =  [self.view subviews];
    UIView *view = nil;
    for (view in views) {
        [view removeFromSuperview];
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

@end
