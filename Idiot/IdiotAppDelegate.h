//
//  IdiotAppDelegate.h
//  Idiot
//
//  Created by preetham d'souza on 1/24/12.
//  Copyright 2012 techjini. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IdiotViewController;

@interface IdiotAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet IdiotViewController *viewController;

@end
